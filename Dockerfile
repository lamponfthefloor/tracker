FROM maven:3-jdk-8

ENV HOME=/home/usr/app
RUN mkdir -p $HOME
WORKDIR $HOME

ADD pom.xml $HOME

RUN ["/usr/local/bin/mvn-entrypoint.sh", "mvn", "verify", "clean", "--fail-never"]

COPY . $HOME

RUN mvn clean package -DskipTests -e

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar", "target/tracker-0.0.1-SNAPSHOT.jar"]