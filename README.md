## Vehicle Tracker Oracle test task ##

#### Task ####

Tracking Vehicles
There are multiple vehicles (~1 mln) in the geographical area with the coordinates known (lat, lon). Vehicles may move fast.

Implement a service that provides RESTful APIs:

* One that returns the list of vehicles located within a given rectangle (top_left_lat, top_left_lng, bottom_right_lat, bottom_right_lng).
* One that allows to track location of a given vehicle (lat, lon).

Vehicles may move fast, so the service should be capable of updating vehicle coordinates reasonably fast (API #2). The service needs to support high number of request to the API #1.
Please develop an application (all-in-one JAR) that implements endpoints mentioned above (with all underlying logic). For simplification persistence for vehicle locations can be skipped.
Please make sure to provide unit tests for the application.

Extra:

* Make application horizontally scalable
* Implement persistence
### Solution ###
#### Technologies: ####

* Spring Boot
* Spring WebFlux
* Spring Data Reactive Repositories with MongoDB
* Docker compose
* Maven

##### Why? #####
###### Spring WebFlux with reactive programming will be able to withstand a much higher load since the server will be non-blocking with its asynchronous nature. Allows for more scalability, a stack immune to latency. ######

###### MongoDB's horizontal, scale-out architecture can support huge volumes of both data and traffic. MongoDB supports massive numbers of reads and writes, one of the best solution for Real-Time Mining of data and Processing. ######
###### With MongoDB Zone sharding features it ensures that the most relevant data reside on shards that are geographically closest to the application servers. Thus tracking vehicle service can be optimized and scaled. I annotated VehicleModel with @Sharded annotation which carries hint and meta information about the distribution of data which can be used lately to optimize application performance and the scalability.


#### How to start the app ####

###### Download Docker.
 Hint: Enable Hyper-V feature on windows and restart;
###### Build and Run containers
     docker-compose up
###### import postman collection from project root
     oracle_test.postman_collection.json
###### import vehicles data
     Postman -> upload #api -> body -> click "remove Vehicles.csv" -> Select Files -> choose Vehicles.csv from root project path
### APIs ###

##### API#1 returns list of vehicles located within a given rectangle
     GET /tracker/vehicles/within-rectangle
Required Parameters with 
* topLeftLat=15.601875
* topLeftLong=-15.556641
* bottomRightLat=6.511815
* bottomRightLong=-3.317871

##### API#2 returns location of a given vehicle
     GET /tracker/vehicle/{id}
Required Parameter id ex.=1

##### Comments #####

In order to test correctness of app 
I used service  [bboxfinder](http://bboxfinder.com/#6.511815,-15.556641,15.601875,-3.317871) and choose lat/long format
Recatngle coordinates 
-15.556641,6.511815,-3.317871,15.601875 with format bottom_left_lng,bottom_left_lat,top_right_lng,top_right_lat 

vehicles which should be within = 1,3,5. 
See data uploaded in file Vehicles.csv
