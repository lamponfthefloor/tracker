package com.oracle.tracker.repository;

import com.oracle.tracker.model.VehicleModel;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;


@Repository
public interface VehicleRepository extends ReactiveMongoRepository<VehicleModel, Long> {

    @Query("{ location: { $geoWithin: { $box: [ [?0, ?1], [?2, ?3] ] } } }")
    Flux<VehicleModel> findVehiclesWithinBox(double bottomLeftLng, double bottomLeftlat,
                                             double topRightLng, double TopRightLat);
}
