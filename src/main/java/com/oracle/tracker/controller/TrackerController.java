package com.oracle.tracker.controller;

import com.oracle.tracker.bean.VehicleData;
import com.oracle.tracker.converter.VehicleConverter;
import com.oracle.tracker.helper.CsvHelper;
import com.oracle.tracker.message.ResponseMessage;
import com.oracle.tracker.service.VehicleService;
import com.oracle.tracker.service.impl.CSVService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/tracker")
@AllArgsConstructor
@Slf4j
public class TrackerController {

    private final VehicleService vehicleService;
    private final VehicleConverter vehicleConverter;
    private final CSVService fileService;
    private final CsvHelper csvHelper;

    @GetMapping(value = "/vehicle/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<VehicleData>> getVehicleLocation(@PathVariable Long id) {

        return vehicleService.findVehicleById(id)
                .map(vehicleConverter::convert)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/vehicles/within-rectangle", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<VehicleData> getVehiclesWithinBox(@RequestParam double topLeftLat, @RequestParam double topLeftLong,
                                                  @RequestParam double bottomRightLat, @RequestParam double bottomRightLong) {

        return vehicleService.findVehiclesWithinBox(topLeftLat, topLeftLong, bottomRightLat, bottomRightLong)
                .map(vehicleConverter::convert);
    }

    //only for test purpose
    @PostMapping("vehicles/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message;

        if (csvHelper.hasCSVFormat(file)) {
            try {
                fileService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
