package com.oracle.tracker.controller;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tracker")
@AllArgsConstructor
@Slf4j
public class HealthCheckController {

    @GetMapping(value = "/home")
    public ResponseEntity<String> getVehicleLocation() {

        return ResponseEntity.ok().build();
    }
}
