package com.oracle.tracker.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Sharded;

@Data
@AllArgsConstructor
@Builder
@Sharded
@Document("vehicles")
public class VehicleModel {

    @Id
    private Long id;

    private GeoJsonPoint location;
}
