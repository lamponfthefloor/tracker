package com.oracle.tracker.service.impl;

import com.oracle.tracker.model.VehicleModel;
import com.oracle.tracker.repository.VehicleRepository;
import com.oracle.tracker.service.VehicleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@Slf4j
@Service
public class DefaultVehicleService implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Override
    public Flux<VehicleModel> findVehiclesWithinBox(double topLeftLat, double topLeftLong,
                                                    double bottomRightLat, double bottomRightLong) {
        //converted to format for geowith box coordinates
        return vehicleRepository.findVehiclesWithinBox(topLeftLong, bottomRightLat, bottomRightLong, topLeftLat);
    }

    @Override
    public Mono<VehicleModel> findVehicleById(Long id) {
        return vehicleRepository.findById(id);
    }

    @Override
    public Flux<VehicleModel> findAllVehicles() {
        return vehicleRepository.findAll();
    }
}
