package com.oracle.tracker.service.impl;


import com.oracle.tracker.helper.CsvHelper;
import com.oracle.tracker.model.VehicleModel;
import com.oracle.tracker.repository.VehicleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@Service
public class CSVService {

    private final VehicleRepository vehicleRepository;
    private final CsvHelper csvHelper;

    public void save(MultipartFile file) {
        try {
            List<VehicleModel> vehicles = csvHelper.csvToVehicles(file.getInputStream());
            vehicleRepository.saveAll(vehicles)
                    .subscribe();
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }

}
