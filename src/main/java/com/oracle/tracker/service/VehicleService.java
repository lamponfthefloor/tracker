package com.oracle.tracker.service;

import com.oracle.tracker.model.VehicleModel;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface VehicleService {

    Flux<VehicleModel> findVehiclesWithinBox(double topLeftLat, double topLeftLong,
                                             double bottomRightLat, double bottomRightLong);

    Mono<VehicleModel> findVehicleById(Long id);

    Flux<VehicleModel> findAllVehicles();
}
