package com.oracle.tracker.converter;

import com.oracle.tracker.bean.VehicleData;
import com.oracle.tracker.model.VehicleModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class VehicleConverter implements Converter<VehicleModel, VehicleData> {

    @Override
    public VehicleData convert(VehicleModel vehicleModel) {
        VehicleData vehicleData = new VehicleData();

        vehicleData.setId(vehicleModel.getId());
        Optional.ofNullable(vehicleModel.getLocation())
                .map(Point::getX)
                .ifPresent(vehicleData::setLongitude);
        Optional.ofNullable(vehicleModel.getLocation())
                .map(Point::getY)
                .ifPresent(vehicleData::setLatitude);

        return vehicleData;
    }
}
