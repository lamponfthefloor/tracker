package com.oracle.tracker.converter;

import com.oracle.tracker.bean.VehicleData;
import com.oracle.tracker.model.VehicleModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class VehicleConverterTest {

    private static final long ID = 1L;
    private static final double LONGITUDE = -5.570068;
    private static final double LATITUDE = 14.827991;
    private VehicleModel vehicleModel;

    @InjectMocks
    private VehicleConverter testingInstance;

    @Before
    public void setUp() {
        GeoJsonPoint location = new GeoJsonPoint(LONGITUDE, LATITUDE);
        vehicleModel = new VehicleModel(ID, location);
    }

    @Test
    public void ShouldReturnVehicleDataWhenConvertCalled() {
        VehicleData expectedVehicleData = VehicleData.builder().id(ID)
                .longitude(LONGITUDE)
                .latitude(LATITUDE)
                .build();

        VehicleData result = testingInstance.convert(vehicleModel);

        assertThat(result).isEqualTo(expectedVehicleData);
    }
}