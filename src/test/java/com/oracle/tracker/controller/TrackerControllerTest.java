package com.oracle.tracker.controller;

import com.oracle.tracker.bean.VehicleData;
import com.oracle.tracker.converter.VehicleConverter;
import com.oracle.tracker.helper.CsvHelper;
import com.oracle.tracker.model.VehicleModel;
import com.oracle.tracker.service.VehicleService;
import com.oracle.tracker.service.impl.CSVService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = TrackerController.class)
public class TrackerControllerTest {

    private static final long VEHICLE_ID_1 = 1L;
    private static final long VEHICLE_ID_2 = 2L;
    private static final double LONGITUDE_1 = -13.161621;
    private static final double LONGITUDE_2 = 13.127629;
    private static final double LATITUDE_1 = -8.887939;
    private static final double LATITUDE_2 = 12.044693;

    @Autowired
    private WebTestClient webTestClient;
    @MockBean
    private CSVService fileService;
    @MockBean
    private CsvHelper csvHelper;
    @MockBean
    private VehicleService vehicleService;
    @MockBean
    private VehicleConverter vehicleConverter;

    private VehicleModel vehicle1;
    private VehicleModel vehicle2;
    private VehicleData vehicleData1;
    private VehicleData vehicleData2;

    @Before
    public void setUp() {
        GeoJsonPoint point1 = new GeoJsonPoint(LONGITUDE_1, LATITUDE_1);
        GeoJsonPoint point2 = new GeoJsonPoint(LONGITUDE_2, LATITUDE_2);
        vehicle1 = new VehicleModel(VEHICLE_ID_1, point1);
        vehicle2 = new VehicleModel(VEHICLE_ID_2, point2);
        vehicleData1 = new VehicleData(VEHICLE_ID_1, LONGITUDE_1, LATITUDE_1);
        vehicleData2 = new VehicleData(VEHICLE_ID_2, LONGITUDE_2, LATITUDE_2);
    }

    @Test
    public void shouldReturnVehicle() {
        Mono<VehicleModel> expectedVehicleModel = Mono.just(vehicle1);
        when(vehicleService.findVehicleById(VEHICLE_ID_1)).thenReturn(expectedVehicleModel);
        when(vehicleConverter.convert(vehicle1)).thenReturn(vehicleData1);

        webTestClient.get()
                .uri("/tracker/vehicle/{id}", VEHICLE_ID_1)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.longitude").isEqualTo(LONGITUDE_1)
                .jsonPath("$.latitude").isEqualTo(LATITUDE_1);
    }

    @Test
    public void shouldGetVehiclesWithinBox() {
        Flux<VehicleModel> expectedVehicleModel = Flux.just(vehicle1, vehicle2);

        double topLeftLat = 15.601875;
        double topLeftLong = -15.556641;
        double bottomRightLat = 6.511815;
        double bottomRightLong = -3.317871;

        when(vehicleService.findVehiclesWithinBox(topLeftLat, topLeftLong, bottomRightLat, bottomRightLong))
                .thenReturn(expectedVehicleModel);
        when(vehicleConverter.convert(vehicle1)).thenReturn(vehicleData1);
        when(vehicleConverter.convert(vehicle2)).thenReturn(vehicleData2);

        webTestClient.get()
                .uri(uriBuilder ->
                        uriBuilder
                                .path("/tracker/vehicles/within-rectangle")
                                .queryParam("topLeftLat", topLeftLat)
                                .queryParam("topLeftLong", topLeftLong)
                                .queryParam("bottomRightLat", bottomRightLat)
                                .queryParam("bottomRightLong", bottomRightLong)
                                .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(VehicleData.class)
                .contains(vehicleData1, vehicleData2);
    }
}