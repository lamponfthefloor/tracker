package com.oracle.tracker.service.impl;

import com.oracle.tracker.model.VehicleModel;
import com.oracle.tracker.repository.VehicleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultVehicleServiceTest {

    @Mock
    private VehicleRepository vehicleRepository;
    @Mock
    private VehicleModel vehicle1;
    @Mock
    private VehicleModel vehicle2;
    @Mock
    private VehicleModel vehicle3;

    @InjectMocks
    private DefaultVehicleService testingInstance;

    @Test
    public void shouldReturnVehiclesWhenFindVehiclesWithinRectangleCalled() {
        Flux<VehicleModel> expectedVehicles = Flux.just(vehicle1, vehicle2);
        double topLeftLat = 15.601875;
        double topLeftLong = -15.556641;
        double bottomRightLat = 6.511815;
        double bottomRightLong = -3.317871;

        when(vehicleRepository.findVehiclesWithinBox(topLeftLong, bottomRightLat, bottomRightLong, topLeftLat))
                .thenReturn(expectedVehicles);

        Flux<VehicleModel> result = testingInstance.findVehiclesWithinBox(topLeftLat, topLeftLong,
                bottomRightLat, bottomRightLong);

        StepVerifier.create(result)
                .expectNext(vehicle1)
                .expectNext(vehicle2)
                .verifyComplete();
    }

    @Test
    public void shouldReturnNothingWhenFindVehiclesWithinRectangleCalled() {
        Flux<VehicleModel> expectedVehicles = Flux.empty();
        double topLeftLat = 15.601875;
        double topLeftLong = -15.556641;
        double bottomRightLat = 6.511815;
        double bottomRightLong = -3.317871;

        when(vehicleRepository.findVehiclesWithinBox(topLeftLong, bottomRightLat, bottomRightLong, topLeftLat))
                .thenReturn(expectedVehicles);

        Flux<VehicleModel> result = testingInstance.findVehiclesWithinBox(topLeftLat, topLeftLong,
                bottomRightLat, bottomRightLong);

        StepVerifier.create(result)
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void shouldReturnAllVehiclesWhenFinAllVehiclesCalled() {
        Flux<VehicleModel> expectedVehicles = Flux.just(vehicle1, vehicle2, vehicle3);

        when(vehicleRepository.findAll())
                .thenReturn(expectedVehicles);

        Flux<VehicleModel> result = testingInstance.findAllVehicles();

        StepVerifier.create(result)
                .expectNext(vehicle1)
                .expectNext(vehicle2)
                .expectNext(vehicle3)
                .verifyComplete();
    }

    @Test
    public void shouldReturnVehicleById() {
        Mono<VehicleModel> expectedVehicle = Mono.just(vehicle1);
        Long vehicleId = 1L;
        when(vehicleRepository.findById(vehicleId)).thenReturn(expectedVehicle);

        Mono<VehicleModel> result = testingInstance.findVehicleById(1L);

        StepVerifier.create(result)
                .expectNext(vehicle1)
                .verifyComplete();
    }
}